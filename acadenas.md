# Recursos de Aurelio Cadenas

Aquí aparece una lista del maravilloso curso de Aurelio Cadenas
sobre [Electrónica Analógica](https://www.youtube.com/@acadenas/playlists?view=50&shelf_id=2).


## Bloque 1

[Bloque 1. Teoría de Circuitos](https://youtu.be/Sfkp81yplPc)

1. [Que es Tension, Corriente, Resistencia, Ley Ohm, Potencia y Energia (Clase 1)](https://youtu.be/57_FZ92uwT8)
1. [Todo sobre los Resistores en electrónica, (Clase 2)](https://youtu.be/jli6YBkRt3U)
1. [Para que sirven y cómo utilizar Leyes de Kirchhoff (Clase 3)](https://youtu.be/xl-eCmWCAzY)
1. [Como resolver circuito por ley tensiones Kirchhoff y Ley de Ohm (Clase 3.1)](https://youtu.be/JFiDufguZbA)
1. [Como utilizar leyes Kichhoff y Ohm para resolver este circuito (Clase 3.2)](https://youtu.be/M8EtFN1_5ik)
1. [Te explico como utilizar Divisores de intensidad y tensión + Ejemplos (Clase 4)](https://youtu.be/Sfkp81yplPc)
1. [Como usar el divisor de tensión (Clase 4.1 Curso Electronica Analogica)](https://youtu.be/Sm2KjO_MBzE)
1. [Como saber la resistencia interna bateria: Reto electronico (Clase 4.2)](https://youtu.be/9KY1WXlAE3k)
1. [Como usar los divisores de tensión en la practica (Clase 4.3)](https://youtu.be/QWwoDewT43A)
1. [Como utilizar Ley Ohm y Divisores de V e I sin hacer operaciones matematicas (Clase 4.4)](https://youtu.be/VL5A_7bZfp8)
1. [Trucos con potenciómetros en electronica. (Clase 5 del curso)](https://youtu.be/4uYnyIRw46g)
1. [Como convertir fuentes de corriente a tensión y viceversa (Clase 6 curso electronica)](https://youtu.be/Dm_fKNuoPP0)
1. [Ejercicios resueltos por conversion fuentes (Clase 6.1 curso electronica)](https://youtu.be/EjUwGgPhl_8)
1. [Clase 7: Puente Wheatstone. Convertir temperatura en tensión()](https://youtu.be/TQve3J-0510)
1. [Conversion Delta-Estrella o triangulo-estrella+Ejercicio (Clase 8 curso electronica)](https://youtu.be/VHNM40ookBw)
1. [La magia de resolver circuito por Millman. (Clase 9 curso electronica basica)](https://youtu.be/cPCXT5OUwgM)
1. [Como usar Teorema de Superposicion para resolver circuitos. (Clase 10)](https://youtu.be/pFbEUhOmges)
1. [Entenderás el teorema de Norton muy fácil (Clase 11 curso electronica)](https://youtu.be/otYl_8qQTAk)
1. [Como usar Thevenin para resolver circuitos (CLASE 12)](https://youtu.be/yuHbKjSdEVA)
1. [Reto electronico: Saber intensidad Puente Wheatstone (clase 12.1)](https://youtu.be/jbaIqgtxpDA)
1. [Como resolver circuito por thevenin y Millman (Curso electronica Clase 12.1)](https://youtu.be/jiTbW6sleM4)
1. [Clase 12.2: Ejercicio resuelto con fuente dependiente por thevenin](https://youtu.be/ZKmfvq3rsks)
1. [Clase 12.3: TRUCO para Thevenin con fuente dependiente](https://youtu.be/0IVNoQC8DyM)
1. [Clase 13: TRUCO para solucionar circuitos por Mallas](https://youtu.be/z6f951TcIdk)
1. [Clase 14: TRUCO Resolver circuito Metodo de LAZOS mejor que Mallas](https://youtu.be/31idalxvY7w)
1. [Clase 15 Método NUDOS o NODOS como no te han explicado](https://youtu.be/f4PXdR_zdnY)
1. [Clase 15.1Circuito resuelto por Nudos con TRUCO](https://youtu.be/yP7oQMhw8Eg)
1. [Clase 15.2 Circuito examen resuelto por NUDOS](https://youtu.be/FuyJhxB_0Vw)
1. [acoplamiento baterias paralelo.Como calcular intensidades y tensiones (clase 15.3)](https://youtu.be/2SNLXKyUk34)
1. [Clase 16: Todos los parametros explicados de la corriente alterna](https://youtu.be/rrr68mtgfCM)
1. [Clase 16.1: Numeros complejos al fin entendí que son.](https://youtu.be/BhX8N6yhUSs)
1. [Clase 16.2: Como operar con números complejos](https://youtu.be/cLHHyK1ik7M)
1. [Como resolver circuito sin hacer calculos matematicos (clase 12.2)](https://youtu.be/Yn6FtUzzPwE)


## Bloque 2

[Bloque 2: CONDENSADOR Y BOBINA en AC/DC](https://youtube.com/playlist?list=PLb_ph_WdlLDlBYwAJYAxQam07kTioUZmR)

1. [Lo basico que debes saber del condensador o capacitor. (Clase 17 curso electronica)](https://youtu.be/Jf14yyhVrAk)
1. [Tipos de Condensadores o capacitores y aplicaciones. (Clase 17.1 curso electronica)](https://youtu.be/8RxiB6t7Rh8)
1. [Como leer condensadores o capacitores. (Clase 17.2 curso electronica)](https://youtu.be/oZ8J9mgo69w)
1. [Que hace un Condensador (capacitor) en DC Parte I. (Clase 18 curso electronica)](https://youtu.be/k9nVIFnkxIE)
1. [Que hace un Condensador o capacitor en DC. Parte II. (Clase 18 curso electronica)](https://youtu.be/N-RlEoNgIbM)
1. [Que hace un condensador en DC. Parte III (Clase 18 )](https://youtu.be/57EwMo4x7D4)
1. [Que hace un condensador en DC. Parte III (Clase 18 )](https://youtu.be/57EwMo4x7D4)
1. [Como funciona Circuito RC con señal cuadrada DC. (Clase 18.1 curso electronica)](https://youtu.be/3DcWhL_ZJRY)
1. [Clase 18.2: Condensadores en serie con DC](https://youtu.be/7HnPH5XBiT4)
1. [Prueba tu conocimiento de electronica: Condensadores serie en DC (Clase 18.3)](https://youtu.be/oNjMTK-OXOQ)
1. [Condensadores paralelo en DC. Clase 18.4)](https://youtu.be/O4cV6k5Y98A)
1. [Clase 19: Que son las Bobinas (Inductores)](https://youtu.be/LXwFKTRQzVY)
1. [Que hace una bobina en DC. Parte I. (Clase 20)](https://youtu.be/7mYAfZMzdv8)
1. [Que hace una bobina en DC. Parte II. (Clase 20 curso electronica)](https://youtu.be/ZcSSSi6go7c)
1. [Como trabajan bobina y condensador juntos en DC(Clase 20.1)](https://youtu.be/vsifku-in2g)
1. [Como trabajan bobina y condensador juntos en DC(Clase 20.1)](https://youtu.be/vsifku-in2g)
1. [Que hace la bobina en AC. (Clase 21 curso electronica)](https://youtu.be/AYQgpRUtLwU)
1. [Que hace un condensador en AC (Clase 22 curso electronica)](https://youtu.be/17Fk2MN9c_Q)
1. [Clase 23: Que hace la resistencia en AC](https://youtu.be/sUYxMDPnGWU)
1. [Que hacen juntos Bobina con resistencia en AC (Clase 24 curso electronica)](https://youtu.be/QoNeZS0Xc7Q)
1. [Clase 24.1: Como medir autoinduccion y resistencia en una bobina](https://youtu.be/cdR5rioDoKo)
1. [Que hacen juntos condensador y resistencia en AC (Clase 25)](https://youtu.be/k0Lgoss3Kdw)
1. [Que potencias hay en corriente alterna AC (Clase 26)](https://youtu.be/KwDjl2Y1xW0)
1. [Potencia reactiva la entenderás muy facil (Clase 26.1)](https://youtu.be/bKXM8W-OaE0)
1. [Factor de potencia. Que es y cómo mejorarlo. (Clase 26.2)](https://youtu.be/pebkVPmUeVY)
1. [Clase 27: Como analizar filtros pasivos paso bajo y paso alto](https://youtu.be/KQvxdAlBj04)


## Bloque 3

[Bloque 3: DIODOS](https://youtube.com/playlist?list=PLb_ph_WdlLDljSWk-9H1cqTLf4eQpPMWq)


1. [Diodo rectificador. Funcionamiento y comprobación (Clase 30 curso electronica)](https://youtu.be/1QQFe_fQ7Oc)
1. [Como saber cuando conduce un diodo. (Clase 30.1 curso electronica basica)](https://youtu.be/gB7ImT50VY0)
1. [Clase 30.2: Circuito limitador con dos diodos](https://youtu.be/3yek_OGj7Bw)
1. [Diodo de proteccion para bobina en DC. (Clase 31 curso electronica)](https://www.youtube.com/watch?v=Euj7xa4WoVE&list=PLb_ph_WdlLDljSWk-9H1cqTLf4eQpPMWq&index=4)
1. [Clase 32: Rectificador de media onda + aplicación](https://youtu.be/ptzroHimUGU)
1. [Como funciona Rectificador con dos diodos y onda completa. (Clase 32.1 curso electronica)](https://youtu.be/DXmvj0qd0RI)
1. [Rectificador en puente. Funcionamiento y comprobacion. (Clase 32.2 curso electronica)](https://youtu.be/XIPhBgh3z_Y)
1. [Como funcionan Limitadores o recortadores de tensión con diodos. (Clase 33 curso electronica)](https://www.youtube.com/watch?v=ZIA28QlQ5hg&list=PLb_ph_WdlLDljSWk-9H1cqTLf4eQpPMWq&index=8)
1. [Como funciona el Sujetador y doblador de tension con diodos. (Clase 33.1)](https://www.youtube.com/watch?v=k2w1bMIf7mw&list=PLb_ph_WdlLDljSWk-9H1cqTLf4eQpPMWq&index=9)
1. [CLase 33.2: Selector de Tensión mayor o tensión menor con diodos](https://youtu.be/LVrmY0woUEg)
1. [Reto electronico: Generador impulsos con onda cuadrada. (Clase 33.3)](https://youtu.be/A-27fEfEfuQ)
1. [No hagas estos 2 Errores en el calculo de potencia (clase 33.4)](https://youtu.be/TVqnYJURm2o)
1. [Como proteger con diodos frente a una polaridad incorrecta (CLase 33.5)](https://youtu.be/g4r5cfbkLD8)



## Bloque 4

[Bloque 4: TRANSISTORES](https://youtube.com/playlist?list=PLb_ph_WdlLDlOFo4m23mgJ5GObPMMVfmI)

1. [Como funciona transistor NPN con detalle (Clase 42)](https://youtu.be/k8v-ukhCc2g)
1. [Como analizar funcionamiento circuito basico transistor NPN. (Clase 42.1 curso electronica)](https://youtu.be/yCPv7Je-D5E)
1. [Como funcionan 4 Circuitos Polarización transistor NPN. (Clase 43 curso electronica basica)](https://youtu.be/pfVUD5FVoB0)
1. [Como funciona circuito temporizador con Transistor NPN. (Clase 44 curso electronica)](https://youtu.be/0Bp0QL9fmA4)
1. [Como funciona y se calcula regulador tension con transistor y zener (Clase 45 curso electronica)](https://youtu.be/K-3kEmexz44)
1. [Como diseñar una fuente intensidad con transistor y zener (Clase 46)](https://youtu.be/2WhU9fqzEXE)
1. [Como funciona transistor PNP con detalle. (Clase 47 curso electronica)](https://youtu.be/5_8AFka51lw)
1. [Como analizar funcionamiento circuito polarización transistor PNP (Clase 47.1 curso electronica)](https://youtu.be/1Cvo0YTPV2g)
1. [Como identificar terminales transistor NPN y PNP (Clase 47.2 )](https://youtu.be/Ic3r9JxoKVg)
1. [Como activar rele con transistor para arduino o raspberry (Clase 48)](https://www.youtube.com/watch?v=TE_pQ8pyL80&list=PLb_ph_WdlLDlOFo4m23mgJ5GObPMMVfmI&index=10)
1. [Como activar con fototransistor un rele para arduino y raspberry (Clase 48.1)](https://www.youtube.com/watch?v=slNPfrQaVlo&list=PLb_ph_WdlLDlOFo4m23mgJ5GObPMMVfmI&index=11)
1. [Cómo elegir resistores para ON OFF Rele con BJT (Clase 48.2)](https://youtu.be/oWf-Hub6_uU)
1. [Como funciona y se repara módulo electrónico protección térmica de motores trifásicos INT69 Kriwan](https://youtu.be/qRpvd3zFcTw)


## Bloque 5

[Bloque 5: AMPLIFICADORES DE POTENCIA](https://youtube.com/playlist?list=PLb_ph_WdlLDkRCJ_BA-f6mtHWr7JGL-N9)

1. [Como evitar quemar los transistores. Calculo y eleccion disipador calor (Clase 54)](https://youtu.be/x8nKy71afas)
1. [Montaje darlington/Darlington complementario (sziklai). Parte I (clase 55)](https://youtu.be/xMqHr51SMmk)
1. [Montaje darlington/Darlington complementario (sziklai). Parte II (clase 55)](https://youtu.be/22bdOCQuNcU)
1. [Como funciona Amplificador potencia Clase A Emisor comun (clase 56)](https://youtu.be/y0995qTUPb0)
1. [Como trabaja el Amplificador potencia clase B y AB (Clase 57)](https://youtu.be/iG-Kr2umHbY)
1. [Amplificador clase D. Como funciona (Clase 59)](https://youtu.be/g9DEaqDQlnI)


## Bloque 6

[Bloque 6: AMPLIFICADORES OPERACIONALES](https://youtube.com/playlist?list=PLb_ph_WdlLDnsLPM53bXz9ebKlsr0h74A)

1. [AMPLIFICADOR OPERACIONAL: Lo que debes saber para empezar (Clase 60)](https://youtu.be/-jNR4824ifo)
1. [Amplificador Operacional como Inversor. Masa virtual bien explicada. (Clase 61)](https://youtu.be/r-YCiCZNA_U)
1. [Como funciona Amplificador NO inversor con OPAMP muy facil (Clase 62)](https://youtu.be/87QHzc4n174)
1. [Como saber la Impedancia entrada y salida de un amplificador inversor (Clase 62.1)](https://www.youtube.com/watch?v=lNbfvl_Pgyo&list=PLb_ph_WdlLDnsLPM53bXz9ebKlsr0h74A&index=4)
1. [Como saber la Impedancia entrada y salida amplificador no inversor (CLase 62.2)](https://youtu.be/mhaa4QElSOw)
1. [Seguidor de tensión con OPAMP. Adaptador de impedancias (Clase 63)](https://youtu.be/Ssglfo5JVwg)
1. [Aplicaciones del sumador inversor con OPAMP (Clase 64)](https://youtu.be/riudPrSbeJs)
1. [Como diseñar Mezclador de audio con OPAMPs (Clase 65)](https://youtu.be/UEDkt36-Q8I)
1. [Circuito regulador velocidad motor DC con amplificadores operacionales (Clase 66)](https://youtu.be/-xokd_mUi_I)
1. [Circuito variar velocidad motor DC (Clase 66.1 Curso electronica analogica)](https://youtu.be/T2Zjyv2E1GQ)


## Bloque 7

[Bloque 7: FUENTES DE TENSION LINEALES](https://youtube.com/playlist?list=PLb_ph_WdlLDn2POke4lOCf0P1KuIKYr6m)

1. [fuente tension estabiliza con zener al detalle (Clase 38)](https://youtu.be/eATKEOlqjkw)
1. [Como funciona circuito regulador tension con OPAMP para fuente tensión(Clase 67)](https://youtu.be/BPiTDtXN40c)
1. [Como elegir resistores para variar voltaje en fuente alimentacion (Clase 67.1 )](https://youtu.be/fGp_8I1uuSQ)
1. [Como diseñar fuentes de tension con regulador 7805 (Clase 90)](https://youtu.be/tyuSi-jFrG0)
1. [Como diseñar Fuente tensión 5V - 3A con 7805 (Clase 90.1)](https://www.youtube.com/watch?v=AgeyaUIAWWM&list=PLb_ph_WdlLDn2POke4lOCf0P1KuIKYr6m&index=5)
1. [Como diseñar fuente de tensión simetrica 12 y -12V con 3A (Clase 91)](https://youtu.be/Ts09VScBMrs)
1. [Fuente de tensión variable de hasta 3A con LM350. Cómo diseñarla (Clase 93)](https://youtu.be/YdbpTqe9_zE)
1. [Como funciona Fuente tension variable 12 a 24V - 2A con LM723 (Clase 94)](https://youtu.be/Zd1vMuEDedo)
1. [Buena fuente tension variable 0 a 30V 2A explicada con detalle (Clase 95)](https://youtu.be/QlyILqPFIXQ)
1. [Como medir intensidad en una PCB sin abrir el circuito (Clase 95.1)](https://youtu.be/doTCNvxfgRo)
1. [Curso Electronica. Limitador de corriente Foldback (CLase 96)](https://youtu.be/Gthqa-_txNc)
1. [Como funciona esta impresionante fuente tension variable 5A (Clase 97)](https://youtu.be/Utr-tJoum8o)
1. [Facilisimo metodo para saber el voltaje de un diodo zener (clase 95.2)](https://youtu.be/IcirLiVjpSo)
1. [Como saber el voltaje y corriente de un LED? Método práctico (Clase 95.3)](https://youtu.be/RIbF9iFGEKE)
1. [SOLUCION RETO: Como identificar avería en Fuente Alimentación (clase 38.1)](https://youtu.be/IUCbZzhHMyw)
1. [Reto electronico: ¿Donde está la avería Fuente tensión 5V - 5A ?(Clase 90.3)](https://youtu.be/D2ersgIoHMs)
1. [SOLUCION Reto electronico Averia en Fuente tension 5V - 5A (Clase 90.3)](https://youtu.be/DDdWJitgHrQ)
1. [Como elegir transformador y fusible en FA lineal (CLASE 90.4)](https://youtu.be/tiirlsWKNbQ)


## Bloque 8

[Bloque 8: FUENTES CONMUTADAS SMPS](https://youtube.com/playlist?list=PLb_ph_WdlLDnvz9kZUSD-BtRX9xO6DZxc)

1. [Como funcionan fuentes alimentación conmutadas SMPS (Clase 100)](https://youtu.be/-zla22UxZE4)
1. [SMPS. Como funciona Circuito protección entrada y filtro EMI (clase 101)](https://youtu.be/J8EAuDA6k8I)
1. [SMPS. Como funcionan circuito Rectificador y filtro primarios (Clase 102)](https://youtu.be/cyoQUnASxQY)
1. [SMPS Como funciona circuito CFP (Corrector Factor Potencia) (Clase 103)](https://youtu.be/guHy24mkRzM)
1. [SMPS: Como funciona convertidor DC DC Flyback (Clase 104)](https://www.youtube.com/watch?v=dGyc4CjXVWU&list=PLb_ph_WdlLDnvz9kZUSD-BtRX9xO6DZxc&index=5)
1. [SMPS. Como funciona Cargador telefono pequeño 5V - 400 mA (Clase 105 )](https://youtu.be/0YO2AEXwiaY)
1. [SMPS. Como funciona adaptador AC-DC 5V - 2A con esquema (Clase 106)](https://youtu.be/pxbrIFPLPF8)
1. [SMPS 5v 12 y -12V Medidas y funcionamiento (Clase 107)](https://www.youtube.com/watch?v=B7vkwEj9jYI&list=PLb_ph_WdlLDnvz9kZUSD-BtRX9xO6DZxc&index=8)
1. [SMPS de 24V - 4A funcionamiento con esquema y fuente conmutada (clase 108)](https://youtu.be/tQspNkt9x38)
1. [ SMPS Cómo funciona cargador batería BARATO (Clase 109)](https://youtu.be/E6A1MUAsof0)
1. [SMPS. Como trabaja circuito flyback con transformador de pulsos (Clase 110.1)](https://youtu.be/eepIQyK0LoQ)
1. [SMPS 12V-10 A explicacion con esquema y fuente conmutada (clase 111)](https://youtu.be/lkqfKvWP24M)
1. [SMPS 200W en semipuente Explicacion funcionamiento (Clase 112)](https://youtu.be/cFoDNeqGEKU)
1. [Como funciona fuente ATX en un PC (clase 113)](https://www.youtube.com/watch?v=K1epC5LEOoM&list=PLb_ph_WdlLDnvz9kZUSD-BtRX9xO6DZxc&index=14)
1. [Como funciona fuente ATX en un PC: Circuito protección y filtro EMI (Clase 114)](https://youtu.be/AYGIxZs7i1w)
1. [Como Funciona Fuente ATX en un PC: Circuito rectificador filtrado y CFP (Clase 115)](https://youtu.be/1YDWofxWUnQ)
1. [Averia fantasma en fuente conmutada SMPS (Clase 108.1)](https://youtu.be/UIlxSn_FhQY)
1. [Como funciona y se repara tu cargador USB telefono 5V - 1A (clase 108.2)](https://youtu.be/4OLeRE8OofQ)
1. [Impresionante circuito Driver bombilla LED con SMPS. Funcionamiento y reparación (clase 109.1)](https://youtu.be/jMuQOf9E6Tw)
1. [Interesante fuente conmutada (SMPS) para laptop EXPLICADA con detalle (Clase 107.1)](https://youtu.be/NOMbAwPOm18)
1. [Te explico como diseñar facil tu propia fuente conmutada SMPS (Clase 110)](https://youtu.be/ZUn-SCqzPZM)
1. [Como hacer transformador para fuente conmutada SMPS (clase 110.1)](https://youtu.be/GicYziNufwE)
1. [Como diseñar muy facil circuito driver lampara LED con SMPS (Clase 109.2)](https://youtu.be/2HzkRcPg1pI)
1. [Como elevar voltaje fuente DC. Funcionamiento convertidor DC DC Boost (elevador)](https://youtu.be/bDjMwM7nD9g)
1. [Como reducir voltaje fuente DC. Funcionamiento Convertidor DC-DC Buck (Reductor) Clase 126](https://youtu.be/fP0ZZpKCxGA)
1. [Como trabaja Red Snubber para semiconductores (Clase 104.1)](https://youtu.be/dxuKWb5kw5c)
1. [Como elegir los valores de un snubber RC (Clase 104.2)](https://youtu.be/egTj4_9QS6c)


## Bloque 9

[Bloque 9: INVERSORES](https://youtube.com/playlist?list=PLb_ph_WdlLDnxtsJv-NRMUzqyJxsFJtZQ)

1. [Circuito inversor basico 12V a 230 V Como es su funcionamiento (clase 116)](https://www.youtube.com/watch?v=zmaLLyM-H1U&list=PLb_ph_WdlLDnxtsJv-NRMUzqyJxsFJtZQ&index=1)
1. [Como trabajar Inversor 12Vdc 230Vac - 600W proteccion bateria (clase 117)](https://www.youtube.com/watch?v=rCbhdW077Bc&list=PLb_ph_WdlLDnxtsJv-NRMUzqyJxsFJtZQ&index=2)
1. [Circuito Inversor lineal 12v a 230 v onda senoidal. Funcionamiento. (Clase 118 )](https://youtu.be/nuEg6IDQo0M)
1. [Puente H: Funcionamiento y aplicaciones (Clase 119)](https://youtu.be/5ChbHiunAxo)
1. [Circuito Inversor SPWM. Funcionamiento (Clase 120)](https://youtu.be/yQOlHsMNt0c)
1. [Circuito Inversor SPWM. Funcionamiento (Clase 120)](https://youtu.be/yQOlHsMNt0c)


## Bloque 10

[Bloque 10: CONVERTIDORES DC-DC](https://youtube.com/playlist?list=PLb_ph_WdlLDmYY5VbMl5GbaO0MC-vuLNY)

1. [Como elevar voltaje fuente DC. Funcionamiento convertidor DC DC Boost (elevador)](https://youtu.be/bDjMwM7nD9g)
1. [Como reducir voltaje fuente DC. Funcionamiento Convertidor DC-DC Buck (Reductor) Clase 126](https://youtu.be/fP0ZZpKCxGA)
1. [Como diseñar tu convertidor DC-DC Buck (reductor) muy fácil (Clase 126.1)](https://youtu.be/rWEZvM6amg0)

## Bloque 11

[Bloque 11: Electronica de Potencia](https://youtube.com/playlist?list=PLb_ph_WdlLDlmDsxPwoRfWMzCs64e2oKy)

1. [Todo sobre AC trifasica. Conceptos basicos (Clase 130)](https://youtu.be/5Jk7CDQuv6Y)
1. [Como medir potencias en trifasica (Clase 131)](https://youtu.be/KdbS7cejTzA)
1. [Cómo saber la secuencia de fases en trifasica sin cálculos matemáticos (Clase 132)](https://youtu.be/ad2GwAXYFrg)
1. [Como mejorar el factor de potencia en trifasica (clase 133)](https://www.youtube.com/watch?v=b4eCSUlvt_M&list=PLb_ph_WdlLDlmDsxPwoRfWMzCs64e2oKy&index=4)
1. [Todo sobre el Rectificador trifasico en estrella (Clase 134)](https://youtu.be/WCHYwlIYvpI)
1. [Todo sobre el Circuito rectificador trifasico en puente (etapa Variador frecuencia) Clase 135](https://youtu.be/wHeF_I1eCko)
1. [Todo lo que debes saber del tiristor SCR (clase 136)](https://youtu.be/mDn5lnaK3tc)
1. [Como funcionan lo circuitos de control de potencia en AC MONOFASICA con tiristores SCR (clase 137)](https://youtu.be/L6XAV5rn6VA)
1. [Como funciona el Rectificador controlado media onda carga inductiva y resistiva (clase 138)](https://youtu.be/cH-elCU1zTM)
1. [Como funciona el rectificador controlado onda completa monofasico (Clase 139)](https://youtu.be/8Y1A3xs2v4A)
1. [Como funciona y se hace un CIRCUITO DISPARO PARA TIRISTORES (CLASE 142)](https://youtu.be/NPlSiou6j7I)

